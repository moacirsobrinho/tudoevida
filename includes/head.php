<head>
     <meta charset="utf-8">
     <title><?php echo $title ?></title>
     <link rel="stylesheet" type="text/css" href="/tudoevida/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" type="text/css" href="/tudoevida/assets/main.css" />
     <script src="/tudoevida/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</head>
