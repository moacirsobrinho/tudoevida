<?php
session_start ();
include_once("conexao.php");

$nome = filter_input(INPUT_POST ,"nome", FILTER_SANITIZE_STRING);
$contato= filter_input(INPUT_POST, "contato", FILTER_SANITIZE_STRING);
$sobrenome= filter_input(INPUT_POST, "sobrenome",FILTER_SANITIZE_STRING);
$cidade = filter_input(INPUT_POST,"cidade",FILTER_SANITIZE_STRING);
$endereco= filter_input(INPUT_POST,"endereco",FILTER_SANITIZE_STRING);
$tipo_sanguineo= filter_input(INPUT_POST, "tipo_sanguineo",FILTER_SANITIZE_STRING);
$cpf= filter_input(INPUT_POST, "cpf",FILTER_SANITIZE_STRING);
$estado= filter_input(INPUT_POST, "estado",FILTER_SANITIZE_STRING);

//Funcao que retorna mensagem de erro caso validacao nâo passe
function validarCampos($name, $city, $blood_type, $cpf){
  $msg = "";
  if(empty($name)){
    $msg = 'Você deve preencher o seu nome.';
    return $msg;
  }
  if(empty($city)){
    $msg = 'Você deve preencher a cidade.';
    return $msg;
  }
  if(empty($blood_type)){
    $msg = 'Você deve preencher o tipo sanguíneo.';
    return $msg;
  }
  if(!validarCPF($cpf)){
    $msg = 'CPF já cadastrado.';
    return $msg;
  }

  return $msg;
}

function validarCPF($cpf){
  global $link;

  $query_busca = "SELECT * FROM usuario WHERE cpf = '$cpf'";
  $resultado_busca = mysqli_query($link, $query_busca);
  if ($resultado_busca->num_rows > 0) {
    return false;
  }else{
    return true;
  }
}

//Valida os campos

//Se estiver tudo OK, insere na base de dados
if(validarCampos($nome, $cidade, $tipo_sanguineo, $cpf) == ''){
  $result_usuario = "INSERT INTO usuario (nome, sobrenome, telefone, cpf, endereco, cidade, estado, tipo_sanguineo) values ('$nome', '$sobrenome', '$contato', '$cpf', '$endereco','$cidade', '$estado', '$tipo_sanguineo')";
  $resultado_usuario = mysqli_query($link, $result_usuario);

  if (mysqli_insert_id($link)) {
      $_SESSION['msg']= "<p style = 'color:white;'>usuario cadastrado com sucesso</p>";
  } else {
      var_dump(mysqli_error($link));exit;
      $_SESSION['msgError']= "<p style = 'color:white;'>usuario não foi cadastrado com sucesso</p>";
  }
  mysqli_close($link);
}else{
  //Caso nao esteja OK, retorna erro para o index.php
  $_SESSION['msgError']= validarCampos($nome, $cidade, $tipo_sanguineo, $cpf);
}

header("location:index.php");
  // code...
 ?>
