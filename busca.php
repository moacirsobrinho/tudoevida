<?php
session_start();
include_once('conexao.php');
$title = 'CRUD - Busca';

//Array de conteudo da busca
$listUsers = array();
$estado = null;
$tipo_sanguineo = null;
//Processamento da busca
if($_POST){
  //Se os dados forem enviados pelo formulário, captura os campos aqui
  $estado = filter_input(INPUT_POST, "estado",FILTER_SANITIZE_STRING);
  $tipo_sanguineo = filter_input(INPUT_POST, "tipo_sanguineo",FILTER_SANITIZE_STRING);

  //Monta a query de busca usando os campos capturados como parametros
  $query_busca = "SELECT * FROM usuario WHERE estado = '$estado' AND tipo_sanguineo='$tipo_sanguineo'";
  $resultado_busca = mysqli_query($link, $query_busca);

  //Retorna o resultado da busca, caso resultado nao venha vazio, itera sobre array
  if ($resultado_busca->num_rows > 0) {
    while($row = $resultado_busca->fetch_assoc()) {
      //Insere cada registro dentro da array de conteudo da busca
      $listUsers[] = array(
        'nome' => $row['nome'],
        'sobrenome' => $row['sobrenome'],
        'tipo_sanguineo' => $row['tipo_sanguineo'],
        'contato' => $row['telefone'],
        'cpf' => $row['cpf'],
        'endereco' => $row['endereco'],
        'cidade' => $row['cidade'],
        'estado' => $row['estado']
      );
    }
  }
}

 ?>
<!DOCTYPE html>
<html lang ="pt_br">
  <?php require('includes/head.php'); ?>
  <body>
    <div class="container">

      <div class="row">
        <?php require('includes/menu.php'); ?>
        <div class="col-md-12">
          <h2 class="titulo">TUDO É VIDA </h2>
          <h4 class="titulo">BUSCA DE DOADORES</h4>
        </div>
        <div class="col-md-12">
          <?php if (isset($_SESSION['msg'])){ ?>
            <span class="error">
              <?php
                echo $_SESSION['msg'];
                unset ($_SESSION['msg']);
              ?>
            </span>
          <?php  } ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 offset-3 mb-4">
            <form method="POST" action="busca.php">
              <div class="form-group">
                <label>Estado</label>
                <select class="form-control" name="estado">
                  <option <?php echo ($estado && $estado == 'AC') ? 'selected' : '' ?> value="AC">Acre</option>
                  <option <?php echo ($estado && $estado == 'AL') ? 'selected' : '' ?>  value="AL">Alagoas</option>
                  <option <?php echo ($estado && $estado == 'AP') ? 'selected' : '' ?>  value="AP">Amapá</option>
                  <option <?php echo ($estado && $estado == 'AM') ? 'selected' : '' ?>  value="AM">Amazonas</option>
                  <option <?php echo ($estado && $estado == 'BA') ? 'selected' : '' ?>  value="BA">Bahia</option>
                  <option <?php echo ($estado && $estado == 'CE') ? 'selected' : '' ?>  value="CE">Ceará</option>
                  <option <?php echo ($estado && $estado == 'DF') ? 'selected' : '' ?>  value="DF">Distrito Federal</option>
                  <option <?php echo ($estado && $estado == 'ES') ? 'selected' : '' ?>  value="ES">Espírito Santo</option>
                  <option <?php echo ($estado && $estado == 'GO') ? 'selected' : '' ?>  value="GO">Goiás</option>
                  <option <?php echo ($estado && $estado == 'MA') ? 'selected' : '' ?>  value="MA">Maranhão</option>
                  <option <?php echo ($estado && $estado == 'MT') ? 'selected' : '' ?>  value="MT">Mato Grosso</option>
                  <option <?php echo ($estado && $estado == 'MS') ? 'selected' : '' ?>  value="MS">Mato Grosso do Sul</option>
                  <option <?php echo ($estado && $estado == 'MG') ? 'selected' : '' ?>  value="MG">Minas Gerais</option>
                  <option <?php echo ($estado && $estado == 'PA') ? 'selected' : '' ?>  value="PA">Pará</option>
                  <option <?php echo ($estado && $estado == 'PB') ? 'selected' : '' ?>  value="PB">Paraíba</option>
                  <option <?php echo ($estado && $estado == 'PR') ? 'selected' : '' ?>  value="PR">Paraná</option>
                  <option <?php echo ($estado && $estado == 'PE') ? 'selected' : '' ?>  value="PE">Pernambuco</option>
                  <option <?php echo ($estado && $estado == 'PI') ? 'selected' : '' ?>  value="PI">Piauí</option>
                  <option <?php echo ($estado && $estado == 'RJ') ? 'selected' : '' ?>  value="RJ">Rio de Janeiro</option>
                  <option <?php echo ($estado && $estado == 'RN') ? 'selected' : '' ?>  value="RN">Rio Grande do Norte</option>
                  <option <?php echo ($estado && $estado == 'RS') ? 'selected' : '' ?>  value="RS">Rio Grande do Sul</option>
                  <option <?php echo ($estado && $estado == 'RO') ? 'selected' : '' ?>  value="RO">Rondônia</option>
                  <option <?php echo ($estado && $estado == 'RR') ? 'selected' : '' ?>  value="RR">Roraima</option>
                  <option <?php echo ($estado && $estado == 'SC') ? 'selected' : '' ?>  value="SC">Santa Catarina</option>
                  <option <?php echo ($estado && $estado == 'SP') ? 'selected' : '' ?>  value="SP">São Paulo</option>
                  <option <?php echo ($estado && $estado == 'SE') ? 'selected' : '' ?>  value="SE">Sergipe</option>
                  <option <?php echo ($estado && $estado == 'TO') ? 'selected' : '' ?>  value="TO">Tocantins</option>
                </select>
              </div>

              <div class="form-group">
                <label>Tipo Sanguíneo</label>
                <select required class="form-control" name="tipo_sanguineo">
                  <option value="">Selecione...</option>
                  <option <?php echo ($tipo_sanguineo && $tipo_sanguineo == 'A+') ? 'selected' : '' ?> value="A+">A+</option>
                  <option <?php echo ($tipo_sanguineo && $tipo_sanguineo == 'A-') ? 'selected' : '' ?> value="A-">A-</option>
                  <option <?php echo ($tipo_sanguineo && $tipo_sanguineo == 'B+') ? 'selected' : '' ?> value="B+">B+</option>
                  <option <?php echo ($tipo_sanguineo && $tipo_sanguineo == 'B-') ? 'selected' : '' ?> value="B-">B-</option>
                  <option <?php echo ($tipo_sanguineo && $tipo_sanguineo == 'AB+') ? 'selected' :'' ?> value="AB+">AB+</option>
                  <option <?php echo ($tipo_sanguineo && $tipo_sanguineo == 'AB-') ? 'selected' :'' ?> value="AB-">AB-</option>
                  <option <?php echo ($tipo_sanguineo && $tipo_sanguineo == 'O+') ? 'selected' : '' ?> value="O+">O+</option>
                  <option <?php echo ($tipo_sanguineo && $tipo_sanguineo == 'O-') ? 'selected' : '' ?> value="O-">O-</option>
                </select>
              </div>

              <input class="btn btn-primary" type="submit" value="BUSCAR DOADORES" />
            </form>
        </div>
      </div>

      <?php if(count($listUsers) > 0){ ?>
      <div class="row">
        <div class="col-md-12">
          <h4>Resultado da Busca (<?php echo count($listUsers) ?> encontrados)</h4>
        </div>
        <div class="col-md-12">
          <table class="table">
            <thead>
              <th>Nome</th>
              <th>sobrenome</th>
              <th>contato</th>
              <th>cpf</th>
              <th>endereco</th>
              <th>cidade</th>
              <th>estado</th>
              <th>tipo_sanguineo</th>
            </thead>
            <tbody>
              <?php foreach($listUsers as $usuario){ ?>
                <tr>
                  <td><?php echo $usuario['nome'] ?></td>
                  <td><?php echo $usuario['sobrenome'] ?></td>
                  <td><?php echo $usuario['contato'] ?></td>
                  <td><?php echo $usuario['cpf'] ?></td>
                  <td><?php echo $usuario['endereco'] ?></td>
                  <td><?php echo $usuario['cidade'] ?></td>
                  <td><?php echo $usuario['estado'] ?></td>
                  <td><?php echo $usuario['tipo_sanguineo'] ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <?php } ?>

    </div>
  </body>
</html>
<?php
             // receber o numero da página
              // $pagina_atual = filter_input( input_get,'pagina',filter_sanitiza_number_int);
              // $pagina_atual = (!empty($pagina_atual) ) $pagina_atual 2;
              //
              // // setar a qualtidade de itens por página
              // $qnt_result_pg = 1;
              //
              // // calcular o inicio da visualização
              // $inicio = ($qnt_result_pg * $pagina )-$qnt_result_pg;
              //
              // $result_usuário ='select * from usuários limit $inicio $qnt_result_pg';
              // $result_usuario = mysql_query ($conn),($result_usuario );
              // while ($row)usuario = mysqli_fetch_assoc($result_usuario){
              //   echo 'id:'.$row_usuario ['id'].'<br>';
              //   echo 'nome:'.$row_usuario['Nome'].'<br>';
              //   echo 'sobrenome:'.$row_usuario['sobrenome'].'<br><br>';
              // }
              //
              // // paginação -somar a quantidade de usuários
              // $result_pg= 'select count,$result (id) as num_result FROM usuario';
              // $result_pg =mysql_query ($conn, $result_pg );
              // $row_pg = mysql_fetch_assoc ($resultado_pg );
              // //echo $row_pg [ 'num_result'];
              // // quantidade de pagina
              // $quantidade_pg= ceil($rom_pg['num_result'] / $qnt_result_pg);
              //
              // // limitar os link antes e depois
              // $max_links = 2;
              // echo '<a href = buscar.php ? pagina=1'> primeira </a>';
              //
              // for ($pag_ant = $pagina -$max_link;$pag_ant <=$pagina - 1;$pag_ant++){
              // if $pag_ant>+1 {
              // echo '<a href ='buscar.php ?pagina= $pag_dep >$pag_ant</> ';

    ?>
