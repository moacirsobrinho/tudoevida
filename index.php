<?php
session_start ();
$title = 'CRUD - Cadastro';
?>
<!DOCTYPE html>
<html lang ="pt-br">
    <?php require('includes/head.php'); ?>
    <body>
      <div class="container">

        <div class="row">
          <?php require('includes/menu.php'); ?>
          <div class="col-md-12">
            <h2 class="titulo">TUDO É VIDA</h2>
            <h4 class="titulo">CADASTRO DE DOADOR</h4>
          </div>
          <div class="col-md-6 offset-3">
            <?php if (isset($_SESSION['msg'])){ ?>
              <span class="success">
                <?php
                  echo $_SESSION['msg'];
                  unset ($_SESSION['msg']);
                ?>
              </span>
            <?php  } ?>
            <?php if (isset($_SESSION['msgError'])){ ?>
              <span class="error">
                <?php
                  echo $_SESSION['msgError'];
                  unset ($_SESSION['msgError']);
                ?>
              </span>
            <?php  } ?>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 offset-3 mb-4">
              <form method="POST" action="processa.php">

                <div class="form-group">
                  <label>Nome</label>
                  <input required class="form-control" type="text" name="nome" placeholder="digite o seu nome" />
                </div>
                <div class="form-group">
                  <label>Sobrenome</label>
                  <input class="form-control" type="text" name="sobrenome" placeholder="digite o seu sobrenome" />
                </div>
                <div class="form-group">
                  <label>Telefone</label>
                  <input class="form-control" type="text" name="contato" placeholder="digite o seu contato"/>
                </div>
                <div class="form-group">
                  <label>Cpf</label>
                  <input class="form-control" type="text" name="cpf" placeholder="digite o seu cpf"/>
                </div>
                <div class="form-group">
                  <label>Endereço</label>
                  <input class="form-control" type="text" name="endereco" placeholder="digite o seu endereço para localização" />
                </div>
                <div class="form-group">
                  <label>Cidade</label>
                  <input class="form-control" type="text" name="cidade" placeholder="digite a cidade onde mora" />
                </div>
                <div class="form-group">
                  <label>Estado</label>
                  <select class="form-control" name="estado">
                    <option value="AC">Acre</option>
                  	<option value="AL">Alagoas</option>
                  	<option value="AP">Amapá</option>
                  	<option value="AM">Amazonas</option>
                  	<option value="BA">Bahia</option>
                  	<option value="CE">Ceará</option>
                  	<option value="DF">Distrito Federal</option>
                  	<option value="ES">Espírito Santo</option>
                  	<option value="GO">Goiás</option>
                  	<option value="MA">Maranhão</option>
                  	<option value="MT">Mato Grosso</option>
                  	<option value="MS">Mato Grosso do Sul</option>
                  	<option value="MG">Minas Gerais</option>
                  	<option value="PA">Pará</option>
                  	<option value="PB">Paraíba</option>
                  	<option value="PR">Paraná</option>
                  	<option value="PE">Pernambuco</option>
                  	<option value="PI">Piauí</option>
                  	<option value="RJ">Rio de Janeiro</option>
                  	<option value="RN">Rio Grande do Norte</option>
                  	<option value="RS">Rio Grande do Sul</option>
                  	<option value="RO">Rondônia</option>
                  	<option value="RR">Roraima</option>
                  	<option value="SC">Santa Catarina</option>
                  	<option value="SP">São Paulo</option>
                  	<option value="SE">Sergipe</option>
                    <option value="TO">Tocantins</option>
                  </select>
                </div>

                <div class="form-group">
                  <label>Tipo Sanguíneo</label>
                  <select required class="form-control" name="tipo_sanguineo">
                    <option value="">Selecione...</option>
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B-">B-</option>
                    <option value="AB+">AB+</option>
                    <option value="AB-">AB-</option>
                    <option value="O+">O+</option>
                    <option value="O-">O-</option>
                  </select>
                </div>
               <input class="btn btn-primary" type="submit" value="CADASTRAR" />
             </form>
             
           </div>
         </div>
       </div>
      </body>
  </html>
